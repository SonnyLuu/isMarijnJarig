<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
	<title>Is Marijn vandaag Jarig?</title>

	<!-- CSS  -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
	<link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
	<?php include_once("analyticstracking.php") ?>
	<header></header>
	<main>

	 <nav class="orange lighten-3 " role="navigation">
		<div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">Is Marijn vandaag jarig?</a>
			

			
			<a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
		</div>
	</nav>

	<div class="section no-pad-bot" id="index-banner">
		<div class="container">
			<div class="row">
				<div class="col s12 l6 offset-l3">
					<div class="card">
						<div class="card-image">
							<!-- <img id="marijnPic1" class="hide" src="img/matrijn.jpg"> -->
							<img id="marijnPic2" class="hide" src="img/hotline.gif">
						</div>
						<div class="card-content">
							<span class="card-title">Is Marijn jarig vandaag?</span>
						
						</div>
						<div class="card-action">
							<a id="ja" class=""></a>
							<a id="nee" class=""></a>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div> 
</main>




<div class="container">
</div>
<br><br>

<div class="section">

</div>
</div>

<footer class="page-footer orange darken-3">
	
	<div class="footer-copyright orange darken-3">
		<div class="container">
			© 2021 
			<a class="grey-text text-lighten-4 right" href="#!"></a>
		</div>
	</div>
</footer>


<!--  Scripts-->

<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script>
	var d = new Date();
	var n = d.getDate();

	var z = new Date();
	var x = z.getMonth()+1;
	console.log("day is: " + n);
	console.log("month is: " + x);

	//if (n == 27 && x == 1)
	if (n == 8 && x == 3)
	{
		$('#ja').text("Ja");
		$('#nee').text("Zeker niet");
		$('#nee').addClass("grey-text text-lighten-2");
		$('#marijnPic2').removeClass("hide");
	}
	else
	{
		$('#ja').text("Ja");
		$('#ja').addClass("grey-text text-lighten-2");
		$('#nee').text("Zeker niet");
		$('#marijnPic1').removeClass("hide");
	};
</script>
<script src="js/materialize.js"></script>
<script src="js/init.js"></script>
<script>
	$(document).ready(function(){
		$('.collapsible').collapsible({
			accordion : false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
		});
	});
</script>

</body>
</html>
